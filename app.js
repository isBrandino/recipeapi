const express = require('express');
const app = express();
const cors = require('cors');
const multer = require('multer');
const requestReference = require('./src/requestHandler'); // Import the class
const session = require('express-session');

require('dotenv').config()
//revert once you push
var whitelist = ['http://localhost:4200/*', 'recipesarehere.com/create', '*s3.amazonaws.com/*',
  'https://recipesarehere.com/*', 'https://www.recipesarehere.com/*', 'www.recipesarehere.com/*'];
if (typeof localStorage === "undefined" || localStorage === null) {
  var LocalStorage = require('node-localstorage').LocalStorage;
  localStorage = new LocalStorage('./scratch');
}
const handlesRequest = new requestReference(localStorage);
var cluster = require('cluster');

if (cluster.isMaster) {
  // Count the machine's CPUs
  var cpuCount = require('os').cpus().length;
  for (var i = 0; i < cpuCount; i += 1) {
    cluster.fork();
  }
  // Listen for dying workers
  cluster.on('exit', function () {
    cluster.fork();
  });
} else {
  app.use(cors());
  app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*', '*/localhost:4200/*', '*/localhost:3002/*', '*/recipesarehere.com/*');

    var host = req.get('host');
    whitelist.forEach(function (val, key) {

      if (host.indexOf(val) > -1) {
        res.setHeader('Access-Control-Allow-Origin', host);
      }
    })

    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();

    app.options('*', (req, res) => {
      res.header('Access-Control-Allow-Methods', 'GET', 'POST', 'PUT');
      res.send();
    });
  });
  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));
  app.use(express.static(__dirname + '/public/images'));
  app.use(session({
    secret: process.env.JWT_SECRET,
    resave: false,
    saveUninitialized: true,
    cookie: { secure: true }
  }));
  const memUpload = multer({ storage: multer.memoryStorage() })

  /* GET */
  app.get('/', (req, res) => handlesRequest.getRoot(req, res));
  app.get('/home', (req, res) => handlesRequest.getHome(req, res));
  app.get('/info', cors(), (req, res) => handlesRequest.getInfo(req, res));
  app.get('/ingr', cors(), (req, res) => handlesRequest.getIngredents(req, res));
  app.get('/steps', cors(), (req, res) => handlesRequest.getSteps(req, res));

  /* POST */
  app.post('/api', cors(), async (req, res) => handlesRequest.postAPI(req, res));
  app.post('/upload', memUpload.single('file'), (req, res) => handlesRequest.postUpload(req, res));
  app.post('/register', cors(), (req, res) => handlesRequest.handleRegistration(req, res));
  app.post('/login', cors(), (req, res) => handlesRequest.handleLogin(req, res));
  app.post('/logout', cors(), async (req, res) => handlesRequest.handleLogout(req, res));
  app.post('/verifyUser', cors(), async (req, res) => handlesRequest.handleUserVerification(req, res));

  

  app.listen(8082, function () {
    console.log(`listening on port 8082...` + `http://localhost:8082/`);
  })
};

// validate  
// cars123 | Cars1cars12@