// promise.js

const request = require('request')

class myPromise{
    constructor(){
        
    }

    promises(url){
        return new Promise((resolve, reject) => {
            request(url, { json: true }, (err, res, body) => {
              if (err) reject(err)
              resolve(body)
            });
        })
    }
}

module.exports.myPromise = myPromise;