// s3.js

var aws = require('aws-sdk');
var crypto = require('crypto');
const util = require('util');
const randomBytes = util.promisify(crypto.randomBytes)

require('dotenv').config()

// .env goes in recipes folder not this one 
const region = "us-east-1";
const bucketName = "alotofwebdatamostlyimages";
const accessKeyId = process.env.AWS_ACCESS_KEY;
const secretAccessKey = process.env.SECRET_AWS_KEY;
const s3 = new aws.S3({
    region,
    accessKeyId,
    secretAccessKey,
    signatureVersion: 'v4'
})

// generateURL - creates url refernce to S3
var generateURL = async function generateURL(){
    const rawBytes = await randomBytes(16)
    const imageName = rawBytes.toString('hex')   
    const params = ({
        Bucket: bucketName,
        Key: imageName,
        Expires: 60
    })
    
    const uploadURL = await s3.getSignedUrlPromise('putObject', params) 
    return uploadURL
}

module.exports.generateURL = generateURL;