// requestHandler.js 

const { v4: uuidv4 } = require('uuid');
const fetch = (...args) => import('node-fetch').then(({ default: fetch }) => fetch(...args));
const AuthService = require('./authService');

var mys3 = require('./s3');
var getreq = require('./db/getRecipe');
var postreq = require('./db/postRecipe');

class RequestHandler {
  // Constructor for initializing any necessary variables or dependencies
  constructor(localStorage) {
    this.reqID;
    this.setID = new Set()
    this.localStorage = localStorage
    this.authService = new AuthService()
  }

  generateKey() {
    this.reqID = uuidv4();
    while (this.setID.has(this.reqID) == true) {
      this.reqID = uuidv4();
    }
    this.reqID = this.reqID.replace(/[^a-z0-9]/gi, '');
  }

  // GET request handler
  getRoot(req, res) {
    getreq.allRecipes((err, result) => {
      if (err) throw err;
      res.set('Content-Type', 'application/json');
      res.end(JSON.stringify(result));
    })
  }

  getHome(req, res) {
    var recipeName = req.query.name;
    if (recipeName === undefined || recipeName === null) {
      res.send({
        "status": "error", "message":
          "Recipe not found"
      })

    } else {
      getreq.getRecipes((err, result) => {

        if (err) throw err;
        let x = JSON.stringify(result);
        res.set('Content-Type', 'application/json');
        res.end(x);
      }, "recipe", "recipeName", recipeName)
    }
  }

  getInfo(req, res) {
    let id = req.query.id;
    if (id === undefined || id === null) {
      res.send({
        "status": "error", "message":
          "Recipe not found"
      })

    } else {
      getreq.getInfo((err, result) => {

        if (err) throw err;
        let x = JSON.stringify(result);
        res.set('Content-Type', 'application/json');
        res.end(x);

      }, id)
    }
  }

  getIngredents(req, res) {
    let id = req.query.id;
    if (id === undefined || id === null) {
      res.send({
        "status": "error", "message":
          "Recipe not found"
      })
    } else {
      getreq.getIngredents((err, result) => {
        if (err) throw err;
        let x = JSON.stringify(result);
        res.set('Content-Type', 'application/json');
        res.end(x);
      }, id)
    }
  }
  getSteps(req, res) {
    let id = req.query.id;
    if (id === undefined || id === null) {
      res.send({
        "status": "error", "message":
          "Recipe not found"
      })
    } else {
      getreq.getSteps((err, result) => {
        if (err) throw err;
        let x = JSON.stringify(result);
        res.set('Content-Type', 'application/json');
        res.end(x);
      }, id)
    }
  }

  // POST request handler
  async postAPI(req, res) {
    try {
      this.generateKey();
      if (req.body === undefined || req.body === '') {
        res.send({
          "status": "error", "message":
            "Recipe not found"
        })
      } else {
        await postreq.addRecipe((err, result) => {
          if (err) throw err;
        }, req.body, this.reqID)

        await postreq.addIngs((err, result) => {
          if (err) throw err;
        }, req.body.ingredients, this.reqID)

        await postreq.addStep((err, result) => {
          if (err) throw err;
        }, req.body.instructions, this.reqID)

        this.localStorage.setItem('idLocal', this.reqID);
      }
    } catch (error) {
      console.error(error);
      res.status(500).send('Internal Server Error');
    }
  }

  async postUpload(req, res) {
    try {
      if (!req.file) {
        console.log("No file received");
        return res.send({
          success: false,
        });
      } else {

        //file exist upload to s3
        const url = await mys3.generateURL()
        //s3 requires a put request
        await fetch(url, {
          method: "PUT",
          headers: {
            "Content-Type": "multipart/form-data"
          },
          body: req.file.buffer
        })

        console.log('file received');

        const imageUrl = url.split('?')[0]
        var theId = this.localStorage.getItem('idLocal')
        await postreq.update((err, result) => {
          if (err) throw err;
        }, imageUrl, theId)

        //store imageUrl in dbs to load it
        return res.send({
          success: true
        })
      }

    } catch (error) {
      console.error(error);
      res.status(500).send('Internal Server Error');
    }
  }

  async handleRegistration(req, res) {
    try {
      const { username, password } = req.body;
      console.log(req.body)
      const message = await this.authService.registerUser(username, password);
      res.status(201).json({ message });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Internal Server Error' });
    }
  }

  async handleLogin(req, res) {
    try {
      const { username, password } = req.body;
      const user = await this.authService.loginUser(username, password);
      const token = this.authService.generateToken(user);
      res.status(200).json({ token });
    } catch (error) {
      console.error(error);
      res.status(401).json({ error: 'Invalid username or password' });
    }
  }

  async handleLogout(req, res) {
    try {
      const message = await this.authService.logoutUser(req);
      res.status(200).json({ message });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Logout failed' });
    }
  }

  async handleUserVerification(req, res) {
    try {
      const token = req.body.token;
      if (!token) {
        return res.status(400).json({ success: false, message: 'Token is missing' });
      }
      const username = await this.authService.verifyToken(token);
      const userDataResponse = await this.authService.fetchAndRespondUserData(username);
      res.status(200).json(userDataResponse);
    } catch (error) {
      console.error('Error handling user verification:', error.message);
      res.status(error.status || 500).json({ success: false, message: error.message || 'Internal server error' });
    }
  }

}

module.exports = RequestHandler;
