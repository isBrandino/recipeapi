// ValidationService.js

class ValidationService {
    static isValidUsername(username) {
      // Validate username allowing alphanumeric characters, underscores, and hyphens.
      const usernameRegex = /^[a-zA-Z0-9_-]+$/;
      return usernameRegex.test(username);
    }
  
    static isValidPassword(password) {
      // Validate password
      const passwordRegex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()-_+=]).{8,100}$/;
      return passwordRegex.test(password);
    }
  }
  
  module.exports = ValidationService;