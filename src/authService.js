// AuthService.js

const jwt = require('jsonwebtoken');
const sqlQueries = require('./db/authQueries');
const ValidationService = require('./validationService');
const bcrypt = require('bcrypt');

class AuthService {
  constructor() {
    sqlQueries.createUsersTableIfNotExists()
    .then((message) => {
      console.log(message);
    })
    .catch((error) => {
      console.error(error);
      process.exit(1); // Exit the application if there's an error during initialization
    });
  }

  async registerUser(username, password) {
    try {
      // Validate username and password
      if (!ValidationService.isValidUsername(username)) {
        throw new Error('Invalid username format');
      }

      if (!ValidationService.isValidPassword(password)) {
        throw new Error('Invalid password format');
      }

      await sqlQueries.insertUser(username, password);
      return 'User registered successfully';
    } catch (error) {
      throw new Error(error);
    }
  }

  async loginUser(username, password) {
    try {
      const results = await sqlQueries.selectUsernameData(username);
      if (results.length === 0) {
        throw new Error('Invalid username or password');
      }

      const user = results[0];
      const passwordMatch = await bcrypt.compare(password, user.password);

      if (passwordMatch) {
        return {
          userId: user.id,
          username: user.username,
          accountType: user.account_type,
        };
      } else {
        throw new Error('Invalid username or password');
      }
    } catch (error) {
      throw new Error(error);
    }
  }

  async logoutUser(req) {
    try {
      await req.session.destroy();
      return 'Logout successful';
    } catch (error) {
      throw new Error('Logout failed');
    }
  }

  hashPassword(password) {
    return bcrypt.hashSync(password, 10);
  }

  comparePasswords(plainPassword, hashedPassword) {
    return bcrypt.compareSync(plainPassword, hashedPassword);
  }

  generateToken(payload) {
    return jwt.sign(payload, process.env.JWT_SECRET , { expiresIn: '1h' });
  }
  
  async verifyToken(token) {
    return new Promise((resolve, reject) => {
      jwt.verify(token, process.env.JWT_SECRET , (err, decoded) => {
        if (err) {
          reject({ status: 403, message: 'Invalid token' });
        } else {
          resolve(decoded.username);
        }
      });
    });
  }

  async fetchAndRespondUserData(username) {
    try {
      const userData = await sqlQueries.selectUserByUsername(username);
      return { success: true, user: userData };
    } catch (error) {
      console.error('Error fetching user data:', error.message);
      throw { status: 500, message: 'Internal server error' };
    }
  }
}

module.exports = AuthService;
