// sqlQueries.js

var mysql = require('mysql');
var mysqlConfig = require('./config.js');
var connection = mysql.createConnection(mysqlConfig);
const { v4: uuidv4 } = require('uuid');
const bcrypt = require('bcrypt');

module.exports = {
  createUsersTableIfNotExists: () => {
    return new Promise((resolve, reject) => {
      try {
        const createTableQuery = `
          CREATE TABLE IF NOT EXISTS users (
            id VARCHAR(36) PRIMARY KEY,
            username VARCHAR(255) NOT NULL,
            password VARCHAR(255) NOT NULL,
            account_type VARCHAR(255) NOT NULL
          )
        `;
        connection.query(createTableQuery, (error) => {
          if (error) {
            console.error(error);
            reject('Failed to create users table');
          }
        });
      } catch (error) {
        console.error(error);
        reject('Internal Server Error');
      }
    });
  },

  insertUser: async (username, password) => {
    try {
      const defaultAccountType = username === 'admin' || username === 'brandino' ? 'admin' : 'noob';
      const hashedPassword = await bcrypt.hash(password, 10);
      const userId = uuidv4();
  
      // Check if the username already exists
      const checkUsernameQuery = 'SELECT COUNT(*) AS count FROM users WHERE username = ?';
      const usernameCheckResult = await new Promise((resolve, reject) => {
        connection.query(checkUsernameQuery, [username], (error, results) => {
          if (error) reject(error);
          else resolve(results);
        });
      });
  
      const count = usernameCheckResult[0].count || 0;
      if (count > 0) {
        throw new Error('Username already exists');
      }
      // Insert the user if the username doesn't exist
      const insertQuery = 'INSERT INTO users (id, username, password, account_type) VALUES (?, ?, ?, ?)';
      await new Promise((resolve, reject) => {
        connection.query(insertQuery, [userId, username, hashedPassword, defaultAccountType], (error) => {
          if (error) reject(error);
          else resolve();
        });
      });
  
      return 'User registered successfully';
    } catch (error) {
      console.error(error);
      throw new Error('User registration failed');
    }
  },

  selectUsernameData: async (username) => {
    return new Promise((resolve, reject) => {
      const selectQuery = 'SELECT * FROM users WHERE username = ?';
      connection.query(selectQuery, [username], (error, results) => {
        if (error) {
          console.error(error);
          reject('Internal Server Error');
        } else {
          resolve(results);
        }
      });
    });
  },

  selectUserByUsername: async (username) => {
    return new Promise((resolve, reject) => {
      const selectQuery = 'SELECT username FROM users WHERE username = ?';
      connection.query(selectQuery, [username], (error, results) => {
        if (error) {
          console.error(error);
          reject('Internal Server Error');
        } else {
          resolve(results);
        }
      });
    });
  },
};
