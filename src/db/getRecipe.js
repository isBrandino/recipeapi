var mysql = require('mysql');
var mysqlConfig = require('./config.js');
var connection = mysql.createConnection(mysqlConfig);
module.exports = connection;

/*

*/
//use prepared statements
var allRecipes = function (callback) {
    connection.query('SELECT recipeName, recipeID, recipe_index, category, info , image_url FROM recipe', (err, result) => {
        if (err) {
            console.log(err);
            callback(err, null);
        }
        callback(null, result);
    });
};
/*

*/
//make sure each recipe id is new
var allID = function (callback) {
    connection.query('SELECT recipeID, info FROM recipe', (err, result) => {
        if (err) {
            console.log(err);
            callback(err, null);
        }
        callback(null, result);
    });

};
/*

*/
//select name description and ingreadents
var getRecipes = function (callback, column, table, value) {
    var sql = "SELECT * FROM ?? WHERE ?? = ?";
    var inserts = [column, table, value];
    sql = mysql.format(sql, inserts);

    connection.query(sql, (err, result) => {
        if (err) {
            console.log(err);
            callback(err, null);
        }
        callback(null, result);
    });

};
/*

*/
var getInfo = function (callback, value) {
    var sql = `SELECT recipe.recipeName, recipe.category, recipe.info, recipe.image_url
        From recipe
        WHERE (recipe.recipeID = ?)`;
    var inserts = [value];
    sql = mysql.format(sql, inserts);

    connection.query(sql, (err, result) => {
        if (err) {
            console.log(err);
            callback(err, null);
        }
        callback(null, result);
    });

};
/*

*/
var getSteps = function (callback, value) {
    var sql = `SELECT *
    From instructions 
    WHERE (recipeID = ?);`;
    var inserts = [value];
    sql = mysql.format(sql, inserts);

    connection.query(sql, (err, result) => {
        if (err) {
            console.log(err);
            callback(err, null);
        }
        callback(null, result);
    });

};
/*

*/
var getIngredents = function (callback, value) {
    var sql = `SELECT *
    From ingredients
    WHERE (recipeID = ?);`;
    var inserts = [value];
    sql = mysql.format(sql, inserts);

    connection.query(sql, (err, result) => {
        if (err) {
            console.log(err);
            callback(err, null);
        }
        callback(null, result);
    });

};

// INSERT INTO `recipe` VALUES ('Rice','Grains',7,0,5,1,0);
// INSERT INTO `instructions` VALUES (6,1,'Cooking Meat for 100 seconds','6i2', null);
module.exports.allID = allID;
module.exports.getInfo = getInfo;
module.exports.getSteps = getSteps;
module.exports.allRecipes = allRecipes;
module.exports.getRecipes = getRecipes;
module.exports.getIngredents = getIngredents;