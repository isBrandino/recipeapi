// post.js

var mysql = require('mysql');
var mysqlConfig = require('./config.js');
var connection = mysql.createConnection(mysqlConfig);
module.exports = connection;

let values = []
let sql;

//POST ingredients
var addRecipe = function (callback, value, myid) {
    var sql = "INSERT INTO `recipe` (recipeName, info, category,  recipeID) VALUES (?,?,?,?)";
    var inserts = [value.name, value.desc, value.category,  myid];
    sql = mysql.format(sql, inserts);
    connection.query(sql, (err, result) => {
        if (err) {
            console.log(err);
            callback(err, null);
        }
        callback(null, result);
    });
};
/*

*/
var update = function (callback, value, myid) {
    var sql = "UPDATE `recipe` SET image_url = ? WHERE recipeID = ?";
    var inserts = [value, myid];
    sql = mysql.format(sql, inserts);
    connection.query(sql, (err, result) => {
        if (err) {
            console.log(err);
            callback(err, null);
        }
        callback(null, result);
    });
};
/*

*/
//for adding instructions
var addStep = function (callback, value, myid) {
    if (value != "") {
        for (let i = 0; i < value.length; i++) {
            var sql = "INSERT INTO `instructions` (recipeID, itemIndex,  instruction) VALUES (?,?,?)";
            inserts = [myid, i, value[i]];
            sql = mysql.format(sql, inserts);
            connection.query(sql, (err, result) => {
                if (err) {
                    console.log(err);
                    callback(err, null);
                }
                callback(null, result);
            });
        }
    }
};
/*

*/
//for adding ingredients
var addIngs = function (callback, value, myid) {
    if (value != "") {
        for (let i = 0; i < value.length; i++) {
            var sql = "INSERT INTO `ingredients` (recipeID, itemIndex, ingredient) VALUES (?,?,?)";
            inserts = [myid, i, value[i]];
            sql = mysql.format(sql, inserts);
            connection.query(sql, (err, result) => {
                if (err) {
                    console.log(err);
                    callback(err, null);
                }
                callback(null, result);
            });
        }
    }
};

module.exports.update = update;
module.exports.addStep = addStep;
module.exports.addIngs = addIngs;
module.exports.addRecipe = addRecipe;