
-- CREATE DATABASE cooking;

-- DROP TABLE instructions;
-- DROP TABLE ingredients;
-- DROP TABLE recipe;
-- CREATE TABLE recipe(
--     recipeName VARCHAR(50) NOT NULL,
--     info VARCHAR(255) NULL,
--     image_url VARCHAR(200) NULL,
--     recipe_index int NOT NULL AUTO_INCREMENT PRIMARY KEY,
--     category VARCHAR(25) null,
--     shares INT NULL DEFAULT '0',
--     recipeID VARCHAR(36)  NOT NULL
-- );

-- run command below to update database 
ALTER TABLE recipe CHANGE likes shares INT NULL DEFAULT '0';

DROP TABLE ingredients;
CREATE TABLE ingredients (
  recipeID  VARCHAR(36),
  itemIndex int NOT NULL DEFAULT '0',
  ingredient VARCHAR(101) NOT NULL
);

DROP TABLE instructions;
CREATE TABLE instructions (
  recipeID  VARCHAR(36),
  itemIndex int NOT NULL DEFAULT '0',
  instruction VARCHAR(1260) NOT NULL
);

--  SELECT * from ingredients, recipe Where (recipe.recipeID = ingredients.recipeID);


DROP TABLE IF EXIST user;
CREATE TABLE user (
  id INT PRIMARY KEY AUTO_INCREMENT,
  username VARCHAR(30) NOT NULL,
  userID INT NOT NULL,
  passwordID VARCHAR(100),
  email VARCHAR(100) UNIQUE,
  secretKey VARCHAR(36) NOT NULL UNIQUE
);
